#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#define HEADER_LINE_MAX 100
#define COMMENT_CHAR '#'
#define RGB_NORMALIZED_MAX 255
#define BLOCK_SIZE 16
#define ME_WINDOW 16

typedef struct {
    int width;
    int height;
    unsigned int maxVal;
    char **pixels;
    unsigned int *count;
} image_data;

const char *get_filename_ext(const char *filename) {
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}

int get_line(FILE *fp, char *line) {
    do{
        char *retVal = fgets(line, HEADER_LINE_MAX, fp);
        if(retVal == NULL) {
            printf ("Error reading line from file! Aborting.\n");
            fclose(fp);
            exit(-5);
        }
    } while(line[0] == COMMENT_CHAR);

    char *comment_char = strchr(line, COMMENT_CHAR);
    if(comment_char != NULL) {
        *comment_char = '\0';
    }

    if(line[strlen(line) - 1] == '\n') {
        line[strlen(line) - 1] = '\0';
    }

    return 0;
}

void check_malloc(void *ptr) {
    if(ptr == NULL) {
        printf("Memory allocation failed! Aborting.\n");
        exit(-11);
    }
}

unsigned short normalize_rgb(int value, int maxValue) {
    int factor = 1;
    if(maxValue > RGB_NORMALIZED_MAX) {
        factor = (maxValue + 1) / (RGB_NORMALIZED_MAX + 1);
        return round((float)value / factor);
    } else if(maxValue < RGB_NORMALIZED_MAX) {
        factor = (RGB_NORMALIZED_MAX + 1) / (maxValue + 1);
        return round((float)value * factor);
    } else {
        return value;
    }
}

void parse_ascii_image_data(FILE *fp, image_data *data) {

    int read, i, j;
    unsigned int value;
    unsigned short  norm;

    for (i = 0; i < data->height; ++i)
    {
        for (j = 0; j < data->width; ++j)
        {
            read = fscanf(fp, "%d", &value);
            if(read != 1) {
                printf("Error reading image data! Aborting.\n");
                fclose(fp);
                exit(-6);
            }
            norm = normalize_rgb(value, data->maxVal);

            (data->pixels)[i][j] = norm;
            (data->count)[norm >> 4]++;
        }
    }
}

void parse_byte_image_data(FILE *fp, image_data *data) {

    unsigned char value;
    unsigned short norm;
    int read, i, j;

    for (i = 0; i < data->height; ++i)
    {
        for (j = 0; j < data->width; ++j)
        {
            read = fread(&value, 1, 1, fp);
            if(read != 1) {
                printf("Error reading image data! Aborting.\n");
                fclose(fp);
                exit(-6);
            }
            norm = normalize_rgb(value, data->maxVal);

            (data->pixels)[i][j] = norm;
            (data->count)[norm >> 4]++;
        }
    }
}

void read_pgm(char *filename, image_data *data) {

    FILE *fp;
    fp = fopen(filename, "rb");
    if(fp == NULL) {
        printf("Error openning file \"%s\"! Aborting.\n", filename);
        exit(-4);
    }

    /* read pgm header */
    char magicNumber[2];
    unsigned int width, height, maxVal;

    char *line = (char *)calloc(HEADER_LINE_MAX, sizeof(char));
    check_malloc(line);

    get_line(fp, line);

    /* try reading all header params from first line */
    unsigned int read, i;

    read = sscanf(line, "%s %d %d %d", magicNumber, &width, &height, &maxVal);

read_header_params:
    switch(read) {
    case 1:
        get_line(fp, line);
        read = sscanf(line, "%d %d %d", &width, &height, &maxVal);
        if(read != 0)
            read += 1;
        goto read_header_params;
        break;
    case 2:
        get_line(fp, line);
        read = sscanf(line, "%d %d", &height, &maxVal);
        if(read != 0)
            read += 2;
        goto read_header_params;
    case 3:
        get_line(fp, line);
        read = sscanf(line, "%d", &maxVal);
        if(read != 1) {
            goto header_error;
        }
        break;
    case 4:
        break;
    default:
    header_error:
        printf("Error while reading ppm header! Aborting.\n");
        fclose(fp);
        exit(-5);
    }

    /* printf("PGM header read; magic: %s, width: %d, height: %d, maxVal: %d\n", magicNumber, width, height, maxVal); */
    free(line);

    int inAscii = 0;

    if(strcmp(magicNumber, "P2") == 0) {
        inAscii = 1;
        /* printf("Image data is in ascii format.\n"); */
    } else if(strcmp(magicNumber, "P5") == 0) {
        inAscii = 0;
        /* printf("Image data is in byte format.\n"); */
        if(maxVal > 255) {
            printf ("Byte format image data supports only sigle byte colours! Aborting.\n");
            fclose(fp);
            exit(-5);
        }
    } else {
        printf("Magic number not recognized, needs to be 'P2' or 'P5' for PGM! Aborting.\n");
        fclose(fp);
        exit(-5);
    }

    if(width % BLOCK_SIZE != 0 || height % BLOCK_SIZE != 0) {
        printf("The image dimensions need to be divisible by %d! Aborting.\n", BLOCK_SIZE);
        fclose(fp);
        exit(-5);
    }

    /* printf("Allocating memory for image data..\n"); */
    data->width = width;
    data->height = height;
    data->maxVal = maxVal;
    data->pixels = malloc(height * sizeof(char *));
    check_malloc(data->pixels);
    for (i = 0; i < height; ++i)
    {
        data->pixels[i] = malloc(width * sizeof(char));
        check_malloc(data->pixels[i]);
    }
    data->count = calloc(16, sizeof(unsigned int));
    check_malloc(data->count);
    /* printf ("Memory alocated.\n"); */

    /* printf("Processing..\n"); */

    if(inAscii) {
        parse_ascii_image_data(fp, data);
    } else {
        parse_byte_image_data(fp, data);
    }

    fclose(fp);

}

// go Hulk
double getMAD(char **first, char **second, int ref_i, int ref_j, int offset_i, int offset_j) {
    unsigned int i, j;
    double diff = 0.0;
    for( i = 0; i < BLOCK_SIZE; i++ ) {
        for( j = 0; j < BLOCK_SIZE; j++ ) {
            diff += fabs(second[ref_i + i][ref_j + j] - first[ref_i + offset_i + i][ref_j + offset_j + j]);
        }
    }

    diff /= (BLOCK_SIZE*BLOCK_SIZE);

    return diff;
}

void estimateMotion(image_data* first, image_data* second, long block) {

    /* find the position of the block */
    unsigned int blocks_in_row = first->width / BLOCK_SIZE;
    int start_row = (block / blocks_in_row) * BLOCK_SIZE;
    int start_col = (block % blocks_in_row) * BLOCK_SIZE;

    int i, j;
    double minMAD = BLOCK_SIZE*BLOCK_SIZE*RGB_NORMALIZED_MAX;
    int minX, minY;
    double tmp;

    for( i = -ME_WINDOW; i <= ME_WINDOW; i++ ) {
        for( j = -ME_WINDOW; j <= ME_WINDOW; j++ ) {

            if(start_row + i >= 0 && start_row + i + ME_WINDOW < first->height && start_col + j >= 0 && start_col + j + ME_WINDOW < first->width) {
                /* printf("calculating (%d, %d)\n", start_row + i, start_col + j); */
                /* fflush(stdout); */
                tmp = getMAD(first->pixels, second->pixels, start_row, start_col, i, j);
                if(tmp < minMAD) {
                    minMAD = tmp;
                    minX = j;
                    minY = i;
                }
            }
        }
    }

    printf("block %ld: (%d, %d)\n", block, minX, minY);
}

int main(int argc, char *argv[]) {

    if(argc != 2) {
        printf("Usage: %s <block>\n", argv[0]);
        return -1;
    }

    char *blockNumber = argv[1];
    char *num;

    long block = strtol(blockNumber, &num, 0);

    if (errno) {
        printf("Error reading block number! Aborting.\n");
        return -3;
    }

    char *firstFile = "lenna.pgm";
    char *secondFile = "lenna1.pgm";

    image_data *dataFirst = (image_data*)malloc(sizeof(image_data));
    check_malloc(dataFirst);
    image_data *dataSecond = (image_data*)malloc(sizeof(image_data));
    check_malloc(dataSecond);

    read_pgm(firstFile, dataFirst);
    read_pgm(secondFile, dataSecond);

    if(dataFirst->width != dataSecond->width || dataFirst->height != dataSecond->height) {
        printf("Both images need to have the same dimension! Aborting.\n");
        exit(-5);
    }

    long blockCount = (dataFirst->width / BLOCK_SIZE) * (dataFirst->height / BLOCK_SIZE);

    if(block >= blockCount) {
        printf("The block number needs to be smaller than the total count of blocks %ld %ld! Aborting.\n", block, blockCount);
        exit(-5);
    }

    estimateMotion(dataFirst, dataSecond, block);

    /* printf("Done.\nWriting output data..\n"); */

    free(dataFirst);
    free(dataSecond);

    return 0;
}
